/*****************************************************************************
 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this file, you
 can obtain one at http://mozilla.org/MPL/2.0/.

 Copyright (C) 2018 Markus Kitsinger (SwooshyCueb) <root@swooshalicio.us>
*****************************************************************************/

var GOOG_CLIENT_ID = '330674672253-78oco6o9f05m5bv0m6t4o2hdahk76663.apps.googleusercontent.com';
var GOOG_DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest"];
var GOOG_SCOPES = 'https://www.googleapis.com/auth/youtube.readonly';

var synth = window.speechSynthesis;

var inputForm = document.querySelector('form');
var inputTxt = document.querySelector('.txt');
var voiceSelect = document.querySelector('select');

var pitch = document.querySelector('#pitch');
var pitchValue = document.querySelector('.pitch-value');
var rate = document.querySelector('#rate');
var rateValue = document.querySelector('.rate-value');

var startBtn = document.getElementById('start');
var stopBtn = document.getElementById('stop');

var voices = [];
var liveChatIds = [];
var channelIds = [];

var unspokenMsgs = [];

function handleClientLoad() {

  populateVoiceList();
  if (speechSynthesis.onvoiceschanged !== undefined) {
    speechSynthesis.onvoiceschanged = populateVoiceList;
  }

  pitch.onchange = function() {
    pitchValue.textContent = pitch.value;
  }

  rate.onchange = function() {
    rateValue.textContent = rate.value;
  }

  inputForm.onsubmit = function(event) {
    event.preventDefault();

    speak();

    inputTxt.blur();
  }

  voiceSelect.onchange = function(){
    speak();
  }


  gapi.load('client:auth2:signin2', initClient);
}

function updateSigninStatus(isSignedIn) {
  if (isSignedIn) {
    getChannel();
  } else {
    channelIds = [];
    liveChatIds = [];
  }
}

function onSignInFail() {
  onSignIn(null);
}

function onSignIn(googUser) {
  channels = [];
  liveChatIds = [];
  updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
}

function initClient() {
  gapi.client.init({
    discoveryDocs: GOOG_DISCOVERY_DOCS,
    clientId: GOOG_CLIENT_ID,
    scope: GOOG_SCOPES
  }).then(function () {
    // Listen for sign-in state changes.
    gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);
    gapi.signin2.render('goog-signin', {
      'scope': GOOG_SCOPES,
      'longtitle': true,
      'theme': 'dark',
      'onsuccess': onSignIn,
      'onfailure': onSignInFail,
    });

    // Handle the initial sign-in state.
    //updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
  });
}

function appendPre(message) {
  var pre = document.getElementById('statustext');
  var textContent = document.createTextNode(message + '\n');
  pre.appendChild(textContent);
}

function getChannel() {
  gapi.client.youtube.channels.list({
    'part': 'snippet',
    'mine': true
  }).then(function(response) {
    for (var cidx = 0; cidx < response.result.items.length; cidx++) {
      var channel = response.result.items[cidx];
      appendPre('Authorized channel "' + channel.snippet.title + '" (' + channel.id + ')');
      appendPre('At present, this service is not very well tested. If something goes wrong, try refreshing the page before panicking.');
      appendPre("If you're using this thing, chances are, you already know who to report bugs to. <3");
      channelIds.push(channel.id);
    }
    getStreams();
  });
}

function getChatMsgs2(liveChatId, pageToken) {
  gapi.client.youtube.liveChatMessages.list({
    'liveChatId': liveChatId,
    'part': 'id,snippet,authorDetails',
    'pageToken': pageToken
  }).then(function(response) {
    setTimeout(function() {getChatMsgs2(liveChatId, response.result.nextPageToken);}, response.result.pollingIntervalMillis);
    for (var midx = 0; midx < response.result.items.length; midx++) {
      var msg = response.result.items[midx];
      if (channelIds.includes(msg.authorDetails.channelId)) {
        appendPre(msg.authorDetails.displayName + ': ' + msg.snippet.displayMessage);
        unspokenMsgs.unshift(msg.snippet.displayMessage);
      }
    }
    speakMsgs();
  });
}

function getChatMsgs(liveChatId) {
  gapi.client.youtube.liveChatMessages.list({
    'liveChatId': liveChatId,
    'part': 'id,snippet,authorDetails'
  }).then(function(response) {
    setTimeout(function() {getChatMsgs2(liveChatId, response.result.nextPageToken);}, response.result.pollingIntervalMillis);
    for (var midx = 0; midx < response.result.items.length; midx++) {
      var msg = response.result.items[midx];
      if (channelIds.includes(msg.authorDetails.channelId)) {
        appendPre(msg.authorDetails.displayName + ' (skipped): ' + msg.snippet.displayMessage);
      }
    }
  });
}

function getStreams() {
  gapi.client.youtube.liveBroadcasts.list({
    'part': 'id,snippet,contentDetails,status',
    'broadcastType': 'all',
    'mine': 'true'
  }).then(function(response) {
    for (var bidx = 0; bidx < response.result.items.length; bidx++) {
      var bcast = response.result.items[bidx];
      // For now, just run until we get the first stream with a liveChatID
      if (bcast.snippet.liveChatId) {
        if (liveChatIds.includes(bcast.snippet.liveChatId)) {
          continue;
        }
        appendPre('Fetching livechat messages from stream "' + bcast.snippet.title + '" (' + bcast.snippet.liveChatId +')')
        liveChatIds.push(bcast.snippet.liveChatId);
        getChatMsgs(bcast.snippet.liveChatId);
      } else {
        continue;
      }
    }
  });
  setTimeout(getStreams, 5000);
}

var populatedOnce = false;
var isSpeaking = false;

function speakMsgs() {
  if (!isSpeaking) {
    isSpeaking = true;
    speakNextMsg();
  }
}

function speakNextMsg() {
  if (synth.speaking) {
    appendPre("speechSynthesis.speaking, something is probably broken oops");
    while(synth.speaking) { }
  }

  if (unspokenMsgs.length <= 0) {
    isSpeaking = false;
    return;
  }

  var msg = unspokenMsgs.pop();
  if (!msg) {
    return speakNextMsg();
  }

  var utterThis = new SpeechSynthesisUtterance(msg);
  utterThis.onend = function(event) {speakNextMsg();}
  utterThis.onerror = function (event) {
    appendPre('SpeechSynthesisUtterance.onerror, something is probably broken oops');
  }

  var selectedOption = voiceSelect.selectedOptions[0].getAttribute('data-name');
  for(i = 0; i < voices.length ; i++) {
    if(voices[i].name === selectedOption) {
      utterThis.voice = voices[i];
    }
  }
  utterThis.pitch = pitch.value;
  utterThis.rate = rate.value;
  synth.speak(utterThis);

  return;
}

function populateVoiceList() {
  if (!populatedOnce) {
    appendPre('Getting available voices...');
  }

  voices = synth.getVoices();
  var selectedIndex = voiceSelect.selectedIndex < 0 ? 0 : voiceSelect.selectedIndex;
  voiceSelect.innerHTML = '';
  for(i = 0; i < voices.length ; i++) {
    var option = document.createElement('option');
    option.textContent = voices[i].name + ' (' + voices[i].lang + ')';

    if(voices[i].default) {
      option.textContent += ' -- DEFAULT';
    }

    option.setAttribute('data-lang', voices[i].lang);
    option.setAttribute('data-name', voices[i].name);
    voiceSelect.appendChild(option);
  }

  voiceSelect.selectedIndex = selectedIndex;


  if (!populatedOnce) {

    var foundHer = false;

    for(i = 0; i < voices.length ; i++) {
      if (voices[i].voiceURI == "Google UK English Female") {
        voiceSelect.selectedIndex = i;
        foundHer = true;
        break;
      }
    }

    if (!foundHer) {
      for(i = 0; i < voices.length ; i++) {
        if (voices[i].lang == "en-GB") {
          voiceSelect.selectedIndex = i;
          foundHer = true;
          //appendPre("Couldn't find Google UK English Female. Set voice to first available en-GB.")
          break;
        }
      }
    }

    if (!foundHer) {
      //appendPre("Couldn't find Google UK English Female.")
    } else {
      populatedOnce = true;
    }
  }
}

function speak(){
  if (synth.speaking) {
    console.error('speechSynthesis.speaking');
    return;
  }
  if (inputTxt.value !== '') {
    var utterThis = new SpeechSynthesisUtterance(inputTxt.value);
    utterThis.onend = function (event) {
        console.log('SpeechSynthesisUtterance.onend');
    }
    utterThis.onerror = function (event) {
        console.error('SpeechSynthesisUtterance.onerror');
    }
    var selectedOption = voiceSelect.selectedOptions[0].getAttribute('data-name');
    for(i = 0; i < voices.length ; i++) {
      if(voices[i].name === selectedOption) {
        utterThis.voice = voices[i];
      }
    }
    utterThis.pitch = pitch.value;
    utterThis.rate = rate.value;
    synth.speak(utterThis);
  }
}
